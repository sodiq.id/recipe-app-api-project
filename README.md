# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables
* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the APP to forward request to (defult: `app`)
* `APP_PORT - Port of the APP to forward request to (defult: `9000`)